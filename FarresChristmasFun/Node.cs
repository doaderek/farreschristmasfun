﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarresChristmasFun
{
    public class Node
    {
        public int Id { get;  }
        public string Value { get; set; }
        public List<Edge> Edges { get; set; }

        public Node(int id)
        {
            Edges = new List<Edge>();
            Id = id;
        }

        public Node(int id, string value)
        {
            Edges = new List<Edge>();
            Value = Value;
            Id = id;
        }
    }
}
