﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarresChristmasFun
{
    public class Edge
    {
        public Node NodeA { get; set; }
        public Node NodeB { get; set; }
        public int Weight { get; set; }

        public Edge(Node nodeA, Node nodeB)
        {
            NodeA = nodeA;
            NodeB = nodeB;
        }


    }
}
