﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarresChristmasFun
{
    public static class Constants
    {
        public static class ExceptionMessages
        {
            public static string EdgeCreatesCycle = "Edge creates a cycle.";
            public static string NodeADoesNotExist = "Node A does not exist.";
            public static string NodeBDoesNotExist = "Node B does not exist.";
        }
    }

}
