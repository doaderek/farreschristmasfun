﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarresChristmasFun
{
    public class Graph
    {
        protected List<Node> Nodes { get; }

        public Graph()
        {
            Nodes = new List<Node>();
        }

        public virtual Node CreateNode()
        {
            var node = new Node(Nodes.Count());
            Nodes.Add(node);
            return node;
        }


        public virtual Edge AddEdge(Node nodeA, Node nodeB)
        {

            int positionA = Nodes.FindIndex(x => x.Id == nodeA.Id);
            int positionB = Nodes.FindIndex(x => x.Id == nodeB.Id);

            if (positionA < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeADoesNotExist);
            if (positionB < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeBDoesNotExist);

            var edge = new Edge(nodeA, nodeB);
            Nodes[positionA].Edges.Add(edge);
            Nodes[positionB].Edges.Add(edge);
            return edge;
        }

    }
}
