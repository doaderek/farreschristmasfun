﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarresChristmasFun
{
    public class DirectedAcyclicGraph : Graph
    {

        public new List<Node> Nodes
        {
            get { return base.Nodes; }
        }

        public DirectedAcyclicGraph() : base() { }

        public override Node CreateNode() { return base.CreateNode(); }


        public override Edge AddEdge(Node nodeA, Node nodeB)
        {
            return AddEdgeKhansWithTopological(nodeA, nodeB);
        }

        public Edge AddEdgeDFSWithTopological(Node nodeA, Node nodeB)
        {
            int positionA = Nodes.FindIndex(x => x.Id == nodeA.Id);
            int positionB = Nodes.FindIndex(x => x.Id == nodeB.Id);

            if (positionA < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeADoesNotExist);
            if (positionB < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeBDoesNotExist);
            if (positionB == positionA)
                throw new ArgumentException(Constants.ExceptionMessages.EdgeCreatesCycle);

            if (positionB > positionA)
            {
                return base.AddEdge(nodeA, nodeB);
            }
            else if (DoesEdgeCreateCycleDFS(nodeA, nodeB))
            {
                throw new ArgumentException(Constants.ExceptionMessages.EdgeCreatesCycle);
            }
            else
            {
                Nodes.RemoveAt(positionA);
                Nodes.Insert(positionB, nodeA);
            }
            return base.AddEdge(nodeA, nodeB);
        }

        public Edge AddEdgeKhansWithTopological(Node nodeA, Node nodeB)
        {
            int positionA = Nodes.FindIndex(x => x.Id == nodeA.Id);
            int positionB = Nodes.FindIndex(x => x.Id == nodeB.Id);

            if (positionA < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeADoesNotExist);
            if (positionB < 0)
                throw new ArgumentException(Constants.ExceptionMessages.NodeBDoesNotExist);
            if (positionB == positionA)
                throw new ArgumentException(Constants.ExceptionMessages.EdgeCreatesCycle);



            var edge = base.AddEdge(nodeA, nodeB);

            if (positionB > positionA)
            {
                return edge;
            }
            else if (IsCyclicalKhans(Nodes.GetRange(positionB, (positionA - positionB) + 1))) {
                throw new ArgumentException(Constants.ExceptionMessages.EdgeCreatesCycle);
            }
            else
            {
                Nodes.RemoveAt(positionA);
                Nodes.Insert(positionB, nodeA);
            }
            return edge;
        }

        public bool IsCyclicalKhans(List<Node> nodes)
        {
            while (RemoveNonNetZeroFlowNodes(nodes) > 0) { }
            return nodes.Count > 0;
        }

        private int RemoveNonNetZeroFlowNodes(List<Node> nodes)
        {
            int nodesRemoved = 0;

            for (int i = nodes.Count - 1; i >= 0; i--)
            {
                bool hasInEdge = false;
                bool hasOutEdge = false;
                var node = nodes[i];
                foreach (var edge in node.Edges)
                {
                    if(edge.NodeA.Id == node.Id && nodes.Any(x => x.Id == edge.NodeB.Id))
                    {
                        hasInEdge = true;
                    }
                    else if(edge.NodeB.Id == node.Id && nodes.Any(x => x.Id == edge.NodeA.Id))
                    {
                        hasOutEdge = true;
                    }
                    if(hasOutEdge && hasInEdge) { break; }
                }
                if (!(hasInEdge && hasOutEdge))
                {
                    nodes.RemoveAt(i);
                    nodesRemoved++;
                }
            }
            return nodesRemoved;
        }


        public bool DoesEdgeCreateCycleBFS(Node nodeA, Node nodeB)
        {
            Queue<Node> nodesToCheck = new Queue<Node>();
            var exploredNodeIds = new HashSet<int>();

            nodesToCheck.Enqueue(nodeB);
            while (nodesToCheck.Count > 0)
            {
                var node = nodesToCheck.Dequeue();
                foreach (var edge in node.Edges)
                {
                    if (edge.NodeA.Id == node.Id)
                    {
                        if (edge.NodeB.Id == nodeA.Id)
                        {
                            return true;
                        }
                        else if (exploredNodeIds.Add(edge.NodeB.Id))
                        {
                            nodesToCheck.Enqueue(edge.NodeB);
                        }

                    }
                }
            }
            return false;
        }
        public bool DoesEdgeCreateCycleDFS(Node startNode, Node endNode, HashSet<int> exploredNodeIds = null)
        {
            exploredNodeIds = exploredNodeIds ?? new HashSet<int>();
            if (endNode.Id == startNode.Id)
            {
                return true;
            }
            else if(exploredNodeIds.Add(endNode.Id))
            {
                foreach (var edge in endNode.Edges)
                {
                    if (edge.NodeA.Id == endNode.Id && DoesEdgeCreateCycleDFS(startNode, edge.NodeB, exploredNodeIds))
                    {
                        return true;
                    }

                }
            }
            return false;

        }
    }
}
