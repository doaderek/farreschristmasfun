﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FarresChristmasFun;

namespace FarresChristmasFun.Tests
{
    [TestClass]
    public class DirectedAcyclicGraphCases
    {

        [TestMethod]
        public void CycleTest()
        {
            var dag = new DirectedAcyclicGraph();
            var node1 = dag.CreateNode();
            var node2 = dag.CreateNode();
            var node3 = dag.CreateNode();

            dag.AddEdge(node1, node2);
            dag.AddEdge(node2, node3);
            try
            {
                dag.AddEdge(node3, node1);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(ex.Message, Constants.ExceptionMessages.EdgeCreatesCycle);
            }

        }

        [TestMethod]
        public void CycleOutOfOrderTest()
        {
            var dag = new DirectedAcyclicGraph();
            var node1 = dag.CreateNode();
            var node2 = dag.CreateNode();
            var node3 = dag.CreateNode();
            var node4 = dag.CreateNode();
            var node5 = dag.CreateNode();
            var node6 = dag.CreateNode();

            dag.AddEdge(node1, node2);
            dag.AddEdge(node2, node3);
            dag.AddEdge(node4, node6);
            dag.AddEdge(node5, node2);
            dag.AddEdge(node4, node5);

            try
            {
                dag.AddEdge(node3, node4);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(ex.Message, Constants.ExceptionMessages.EdgeCreatesCycle);
            }

        }

        [TestMethod]
        public void CycleFragmentedTest()
        {
            var dag = new DirectedAcyclicGraph();
            var node1 = dag.CreateNode();
            var node2 = dag.CreateNode();
            var node3 = dag.CreateNode();
            var node4 = dag.CreateNode();
            var node5 = dag.CreateNode();
            var node6 = dag.CreateNode();

            dag.AddEdge(node1, node2);
            dag.AddEdge(node3, node4);
            dag.AddEdge(node5, node6);
            dag.AddEdge(node2, node5);
            dag.AddEdge(node5, node3);
            dag.AddEdge(node6, node3);
            dag.AddEdge(node1, node4);

            try
            {
                dag.AddEdge(node4, node5);
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(ex.Message, Constants.ExceptionMessages.EdgeCreatesCycle);
            }

        }

        [TestMethod]
        public void NoCycleFragmentedTest()
        {
            var dag = new DirectedAcyclicGraph();
            var node1 = dag.CreateNode();
            var node2 = dag.CreateNode();
            var node3 = dag.CreateNode();
            var node4 = dag.CreateNode();
            var node5 = dag.CreateNode();
            var node6 = dag.CreateNode();

            dag.AddEdge(node1, node2);
            dag.AddEdge(node3, node4);
            dag.AddEdge(node5, node6);
            dag.AddEdge(node2, node5);
            dag.AddEdge(node5, node3);
            dag.AddEdge(node6, node3);
            dag.AddEdge(node1, node4);
            dag.AddEdge(node2, node3);


        }

        [TestMethod]
        public void NoCycleComplexTest()
        {
            var dag = new DirectedAcyclicGraph();
            int max = 300;
            for(int i = 0; i< max; i++)
            {
                dag.CreateNode();
            }

            for (int i = 0; i < max; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    if(j>i)
                        dag.AddEdge(dag.Nodes.Find(x => x.Id == j), dag.Nodes.Find(x => x.Id == i));
                }
            }
        }

        [TestMethod]
        public void CycleComplexTest()
        {
            var dag = new DirectedAcyclicGraph();
            int max = 300;
            for (int i = 0; i < max; i++)
            {
                dag.CreateNode();
            }

            for (int i = 0; i < max; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    if (j > i)
                        dag.AddEdge(dag.Nodes.Find(x => x.Id == j), dag.Nodes.Find(x => x.Id == i));
                }
            }

            try
            {
                dag.AddEdge(dag.Nodes.Find(x => x.Id == 0), dag.Nodes.Find(x => x.Id == max-1));
                Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(ex.Message, Constants.ExceptionMessages.EdgeCreatesCycle);
            }
        }


    }
}
